import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { platformInitialization } from './platform-initialization';

if (environment.production) {
  enableProdMode();
}

platformInitialization()
  .then((extraProviders) => platformBrowserDynamic(extraProviders).bootstrapModule(AppModule))
  .catch((err) => console.error(err));
