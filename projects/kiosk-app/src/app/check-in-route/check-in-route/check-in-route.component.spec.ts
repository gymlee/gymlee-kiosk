import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckInRouteComponent } from './check-in-route.component';

describe('CheckInRouteComponent', () => {
  let component: CheckInRouteComponent;
  let fixture: ComponentFixture<CheckInRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CheckInRouteComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckInRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
