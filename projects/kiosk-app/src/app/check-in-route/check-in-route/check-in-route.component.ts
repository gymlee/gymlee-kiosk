import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { KioskCheckInService } from '../../kiosk/kiosk-check-in.service';

@Component({
  selector: 'app-check-in-route',
  templateUrl: './check-in-route.component.html',
  styleUrls: ['./check-in-route.component.scss'],
})
export class CheckInRouteComponent implements OnInit {
  isFocus = false;

  form = new FormGroup({
    cardCode: new FormControl(''),
  });

  constructor(private readonly kioskCheckInService: KioskCheckInService) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const cardCode = this.form.get('cardCode')!.value;
    if (!cardCode) {
      return;
    }

    const timestamp = new Date().toISOString();
    this.kioskCheckInService.create(timestamp, cardCode, null).subscribe(() => {
      this.openSnackBar();
      this.launchRipple();
    });
    this.form.reset();
  }

  onFocus(): void {
    this.isFocus = true;
  }

  onBlur(): void {
    this.isFocus = false;
  }

  private openSnackBar(): void {
    console.log('TODO open snack bar');
  }

  private launchRipple(): void {
    console.log('TODO launch ripple');
  }
}
