import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckInRouteComponent } from './check-in-route/check-in-route.component';
import { CheckInRouteRoutingModule } from './check-in-route-routing.module';
import { MatCardModule } from '@angular/material/card';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [CheckInRouteComponent],
  imports: [
    CommonModule,
    CheckInRouteRoutingModule,
    MatCardModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
  ],
})
export class CheckInRouteModule {}
