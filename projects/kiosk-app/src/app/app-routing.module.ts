import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'check-in-route',
    loadChildren: () => import('./check-in-route/check-in-route.module').then((m) => m.CheckInRouteModule),
  },
  {
    path: '**',
    redirectTo: 'check-in-route',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
