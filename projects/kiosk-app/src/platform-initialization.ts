import { StaticProvider } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { environment } from './environments/environment';

export async function platformInitialization(): Promise<StaticProvider[]> {
  /**
   * KeycloakService is provided in platform Injector, since there should be exactly one Keycloak instance on the page.
   * Also, Keycloak initialization runs before AppModule bootstrap so any auth-related redirect happens as soon as
   * possible. Redirects happen since not every browser supports silent check-sso - e.g. Chrome prevents it when
   * browsing incognito.
   */
  const keycloakService = await createKeycloakService();
  const keycloakServiceProvider: StaticProvider = {
    provide: KeycloakService,
    useValue: keycloakService,
  };

  return [keycloakServiceProvider];
}

async function createKeycloakService(): Promise<KeycloakService> {
  /**
   * It would be better to just call init and eventually handle an error. But in case of no internet connection, Promise
   * returned from init just never settle.
   */
  if (await isKeycloakReachable()) {
    const keycloakService = new KeycloakService();
    await keycloakService.init({
      config: {
        url: environment.keycloakUrl,
        realm: environment.keycloakRealm,
        clientId: environment.keycloakClientId,
      },
      initOptions: {
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: window.location.origin + '/assets/silent-check-sso.html',
      },
    });
    return keycloakService;
  } else {
    // TODO if cannot initialize now, probably should somehow do it later; custom KeycloakService may be necessary
    return new KeycloakService();
  }
}

/**
 * Better solution would be to download keycloak.js from Keycloak, but currently keycloak-angular has hardcoded use of
 * keycloak-js npm dependency. There is an idea to use paths in tsconfig to override keycloak-js and return Keycloak
 * loaded with script tag. Although cannot just put script tag in index.html since Keycloak url is in environment.ts.
 */
async function isKeycloakReachable(): Promise<boolean> {
  try {
    const response = await fetch(`${environment.keycloakUrl}/realms/${environment.keycloakRealm}?ngsw-bypass`);
    return response.ok;
  } catch (error) {
    console.error(error);
    return false;
  }
}
