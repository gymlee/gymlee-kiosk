export const environment = {
  production: true,
  keycloakUrl: 'https://auth.gymlee.com/auth',
  keycloakRealm: 'kiosk',
  keycloakClientId: 'kiosk-app',
};
